[![Build Status](https://travis-ci.org/OCA/vertical-isp.svg?branch=12.0)](https://travis-ci.org/OCA/vertical-isp)
[![Coverage Status](https://coveralls.io/repos/OCA/vertical-isp/badge.png?branch=12.0)](https://coveralls.io/r/OCA/vertical-isp?branch=12.0)

# Vertical ISP

[//]: # (addons)

Available addons
----------------
addon | version | summary
--- | --- | ---
[connector_equipment](connector_equipment/) | 12.0.1.0.0 | Connect Equipment to Outside API
[connector_equipment_service](connector_equipment_service/) | 12.0.1.0.0 | Combine Connector Equipment and Service Profiles

[//]: # (end addons)

## Translation Status

[![Translation status](https://translation.odoo-community.org/widgets/vertical-isp-12-0/-/multi-auto.svg)](https://translation.odoo-community.org/engage/vertical-isp-12-0/?utm_source=widget)

----

OCA, or the Odoo Community Association, is a nonprofit organization whose 
mission is to support the collaborative development of Odoo features and 
promote its widespread use.

http://odoo-community.org/
